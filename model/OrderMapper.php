<?php

require_once __DIR__.'/Order.php';
require_once __DIR__.'/../Database.php';

class OrderMapper
{
    private $database;

    public function __construct()
    {
        $this->database = new Database();
    }

    public function getOrder($id)
    {
        try {
            $stmt = $this->database->connect()->prepare('SELECT * FROM orders WHERE ordersID = :id;');
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt->execute();

            $order = $stmt->fetch(PDO::FETCH_ASSOC);
            return new Order($order['id'], $order['name'], $order['image']);
        }
        catch(PDOException $e) {
            return 'Error: ' . $e->getMessage();
        }
    }
}