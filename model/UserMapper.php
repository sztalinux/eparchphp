<?php

require_once 'User.php';
require_once __DIR__.'/../Database.php';



class UserMapper
{
    private $database;

    public function __construct()
    {
        $this->database = new Database();
    }

    public function getUser(string $email):User {
        try {
            $stmt = $this->database->connect()->prepare('SELECT * FROM users WHERE email = :email;');
            $stmt->bindParam(':email', $email, PDO::PARAM_STR);
            $stmt->execute();

            $user = $stmt->fetch(PDO::FETCH_ASSOC);
            return new User($user['id'], $user['name'], $user['surname'], $user['nick'], $user['email'], $user['password'], $user['role'], $user['team']);
        }
        catch(PDOException $e) {
            return 'Error: ' . $e->getMessage();
        }
    }

    public function getUserByID($id):User {
        try {
            $stmt = $this->database->connect()->prepare('SELECT * FROM users WHERE id = :id;');
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt->execute();

            $user = $stmt->fetch(PDO::FETCH_ASSOC);
            return new User($user['id'], $user['name'], $user['surname'], $user['nick'], $user['email'], $user['password'], $user['role'], $user['team']);
        }
        catch(PDOException $e) {
            return 'Error: ' . $e->getMessage();
        }
    }

    public function setUser(string $name, string $surname, string $nick, string $email, string $password, int $role = ROLE_SCAB, string $team = null) {
        try {
            $pdo = $this->database->connect();
            $pdo->beginTransaction();

            $stmt = $pdo->prepare("INSERT INTO users (name, surname, nick, email, password, role, team)
                                                                    VALUES(:name, :surname, :nick, :email, :password, :role, :team)");
            $stmt->bindParam(':name', $name, PDO::PARAM_STR);
            $stmt->bindParam(':surname', $surname, PDO::PARAM_STR);
            $stmt->bindParam(':nick', $nick, PDO::PARAM_STR);
            $stmt->bindParam(':email', $email, PDO::PARAM_STR);
            $stmt->bindParam(':password', $password, PDO::PARAM_STR);
            $stmt->bindParam(':role', $role, PDO::PARAM_INT);
            $stmt->bindParam(':team', $team, PDO::PARAM_STR); //utworzyc TEAM i TEAMMAPPER i włożyć to do tego
            $stmt->execute();

            $pdo->commit();
        }
        catch(PDOException $e) {
            $pdo->rollBack();
            return 'Error: ' . $e->getMessage();
        }
    }

    public function checkIfEmailExists(string $email) {
        try {
            $stmt = $this->database->connect()->prepare('SELECT * FROM users WHERE email = :email;');
            $stmt->bindParam(':email', $email, PDO::PARAM_STR);
            $stmt->execute();

            $user = $stmt->fetch(PDO::FETCH_ASSOC);

            if($user['email'] == $email)
                return True;
            else
                return False;
        }
        catch(PDOException $e) {
            return 'Error: ' . $e->getMessage();
        }
    }


    public function getAllUsers()
    {
        try {
            $stmt = $this->database->connect()->prepare('SELECT * FROM users WHERE email != :email;');
            $stmt->bindParam(':email', $_SESSION['email'], PDO::PARAM_STR);
            $stmt->execute();

            $users = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $users;
        }
        catch(PDOException $e) {
            die();
        }
    }

    public function delete(int $id): void
    {
        try {
            $stmt = $this->database->connect()->prepare('DELETE FROM users WHERE id = :id;');
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
        }
        catch(PDOException $e) {
            die();
        }
    }

    public function getSubordinates($userRole)
    {
        try {
            $role = $userRole;
            $stmt = $this->database->connect()->prepare('SELECT * FROM subordinates WHERE roleID > :role;');
            $stmt->bindParam(':role',$role, PDO::PARAM_INT);
            $stmt->execute();

            $users = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $users;
        }
        catch(PDOException $e) {
            die();
        }
    }
}