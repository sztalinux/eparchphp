<?php

require_once __DIR__.'/Order.php';
require_once __DIR__.'/User.php';
require_once __DIR__.'/Achievement.php';
require_once __DIR__.'/../Database.php';

class AchievementMapper
{
    private $database;

    public function __construct()
    {
        $this->database = new Database();
    }

    public function getUserAchievements($id)
    {
        try {
            $stmt = $this->database->connect()->prepare('SELECT * FROM achievementsToDisplay WHERE userID = :id;');
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt->execute();

            $achievements = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $achievements;
        }
        catch(PDOException $e) {
            return 'Error: ' . $e->getMessage();
        }
    }
}