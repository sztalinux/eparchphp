<?php
/**
 * Created by PhpStorm.
 * User: Albert
 * Date: 2/6/2019
 * Time: 7:37 PM
 */

class Achievement
{
    private $id;
    private $user;
    private $order;
    private $achieveDate;
    private $description;
    private $author;

    public function __construct($id, $user, $order, $achieveDate, $description, $author)
    {
        $this->id = $id;
        $this->user = $user;
        $this->order = $order;
        $this->achieveDate = $achieveDate;
        $this->description = $description;
        $this->author = $author;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user): void
    {
        $this->user = $user;
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function setOrder($order): void
    {
        $this->order = $order;
    }

    public function getAchieveDate()
    {
        return $this->achieveDate;
    }

    public function setAchieveDate($achieveDate): void
    {
        $this->achieveDate = $achieveDate;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description): void
    {
        $this->description = $description;
    }

    public function getAuthor()
    {
        return $this->author;
    }

    public function setAuthor($author): void
    {
        $this->author = $author;
    }


}