<?php

class User
{
    private $id;
    private $name;
    private $surname;
    private $nick;
    private $email;
    private $password;
    private $role = ROLE_SCAB;
    private $team = null;



    public function __construct($id, $name, $surname, $nick, $email, $password, $role, $team)
    {
        $this->id = $id;
        $this->name = $name;
        $this->surname = $surname;
        $this->nick = $nick;
        $this->email = $email;
        $this->password = $password;
        $this->role = $role;
        $this->team = $team;

    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    public function getNick()
    {
        return $this->nick;
    }

    public function setNick($nick)
    {
        $this->nick = $nick;
    }
    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = md5($password);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getRole(): int
    {
    return $this->role;
    }

    public function setRole(int $role)
    {
        $this->role = $role;
    }
    public function getRoleName(): string
    {
        if ($this->role == ROLE_ADMIN){
            return "Admin";
        }
        if ($this->role == ROLE_CHIEF){
            return "Wódz";
        }
        if ($this->role == ROLE_ELDER){
            return "Starszy";
        }
        if ($this->role == ROLE_SCAB){
            return "Parch";
        }
    }

    public function getTeam()
    {
        return $this->team;
    }

    public function setTeam($team): void
    {
        $this->team = $team;
    }

}
