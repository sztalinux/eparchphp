<?php
require_once 'AppController.php';

require_once __DIR__.'/../model/User.php';
require_once __DIR__.'/../model/UserMapper.php';

class ElderController extends AppController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if($this->roleValidation(ROLE_ELDER)){
            exit();
        }

        $user = new UserMapper();
        $this->render('index', ['user' => $user->getUser($_SESSION['email'])]);

    }

    public function subordinates(): void
    {
        if($this->roleValidation(ROLE_ELDER)){
            exit();
        }

        $user = new UserMapper();

        header("Content-type: application/json charset=utf-8");
        http_response_code(200);
//        $user->getSubordinates(ROLE_ELDER);
        echo $user->getSubordinates(ROLE_ELDER) ? json_encode($user->getSubordinates(ROLE_ELDER)) : '';
        die();
    }

    public function subordinatesView()
    {
        if($this->roleValidation(ROLE_ELDER)){
            exit();
        }

        $this->render('subordinates');
    }

    public function menu()
    {
        if($this->roleValidation(ROLE_ELDER)){
            exit();
        }
        $this->render('menu');
    }
}