<?php

require_once 'AppController.php';

require_once __DIR__.'/../model/User.php';
require_once __DIR__.'/../model/UserMapper.php';

class AdminController extends AppController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(): void
    {

        if($this->roleValidation(ROLE_ADMIN)){
            exit();
        }

        $user = new UserMapper();
        $this->render('index', ['user' => $user->getUser($_SESSION['email'])]);
    }

    public function users(): void
    {
        $user = new UserMapper();

        header('Content-type: application/json');
        http_response_code(200);

        echo $user->getAllUsers() ? json_encode($user->getAllUsers()) : '';
    }

    public function userDelete(): void
    {
        if($this->roleValidation(ROLE_ADMIN)){
            exit();
        }

        if (!isset($_POST['id'])) {
            http_response_code(404);
            return;
        }

        $user = new UserMapper();
        $user->delete((int)$_POST['id']);

        http_response_code(200);
    }
}