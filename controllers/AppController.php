<?php

class AppController
{
    private $request = null;

    public function __construct()
    {
        $this->request = strtolower($_SERVER['REQUEST_METHOD']);
        session_start();
    }

    public function isGet()
    {
        return $this->request === 'get';
    }

    public function isPost()
    {
        return $this->request === 'post';
    }

    public function render(string $fileName = null, $variables = [], string $controller = 'controller')
    {
        if($controller == 'controller'){
            $controller = get_class($this);
        }
        $view = $fileName ? dirname(__DIR__).'/views/'.$controller.'/'.$fileName.'.php' : '';

        $output = 'There isn\'t such file to open';

            if (file_exists($view)) {

                extract($variables); //wypakowywuje z tablicy na zmienne, gdzie klucz -> nazwa zmiennej;

                ob_start();
                include $view;
                $output = ob_get_clean();
            }

        print $output;
    }
    public function roleValidation($ROLE){
        if ($_SESSION['role'] != $ROLE) {
            $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}?page=home");
            return True;
        }else return False;
    }
//    function userRoleValidation($object, $ROLE, $whereIfProper, $whereIfWrong, $data = [], string $controller = 'controller'){
//
//        if ($_SESSION['role'] == $ROLE) {
//            $user = new UserMapper();
//            $object->render($whereIfProper, ['user' => $user->getUser($_SESSION['id'])]);
//        } else {
//            $url = "http://$_SERVER[HTTP_HOST]/";
//            header("Location: {$url}?page=$whereIfWrong");
//        }
//    }
}

