<?php
/**
 * Created by PhpStorm.
 * User: Albert
 * Date: 2/7/2019
 * Time: 8:23 PM
 */

class MyAccountController extends AppController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if(!isset($_SESSION['role'])){
            exit();
        }

        $user = new UserMapper();
        $this->render('index', ['user' => $user->getUser($_SESSION['email'])]);
    }

    public function menu()
    {
        if($_SESSION['role'] == ROLE_SCAB){
            $this->render('menu', [],'ScabController');
        }
        if($_SESSION['role'] == ROLE_ELDER){
            $this->render('menu', [],'ElderController');
        }
        if($_SESSION['role'] == ROLE_CHIEF){
            $this->render('menu', [],'ChiefController');
        }

    }
}