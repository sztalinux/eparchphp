<?php

require_once "AppController.php";

require_once __DIR__ . '/../model/User.php';
require_once __DIR__ . '/../model/UserMapper.php';


class HomeController extends AppController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if (isset($_SESSION) && !empty($_SESSION)) {
            $this->render('index');
        } else {
            $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}?page=welcome");
        }
    }
}
