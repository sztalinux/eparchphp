<?php
require_once 'AppController.php';

require_once __DIR__.'/../model/User.php';
require_once __DIR__.'/../model/UserMapper.php';
require_once __DIR__.'/../model/Achievement.php';
require_once __DIR__.'/../model/AchievementMapper.php';

class ScabController extends AppController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function orders()
    {
        if($this->roleValidation(ROLE_SCAB)){
            exit();
        }

        $achievements = new AchievementMapper();
        header("Content-type: application/json");
        http_response_code(200);

        echo $achievements->getUserAchievements($_SESSION['id']) ? json_encode($achievements->getUserAchievements($_SESSION['id'])) : '';
    }

    public function ordersListView()
    {
        if($this->roleValidation(ROLE_SCAB)){
            exit();
        }

        $this->render('ordersList');
    }

    public function menu()
    {
        if($this->roleValidation(ROLE_SCAB)){
            exit();
        }
        $this->render('menu');
    }

    public function events()
    {
        if($this->roleValidation(ROLE_SCAB)){
            exit();
        }
        $user = new UserMapper();
        $this->render('events');
    }
}