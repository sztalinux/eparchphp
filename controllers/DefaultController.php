<?php

require_once "AppController.php";

require_once __DIR__ . '/../model/User.php';
require_once __DIR__ . '/../model/UserMapper.php';


class DefaultController extends AppController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function welcome()
    {
        if (!isset($_SESSION) or empty($_SESSION)) {
            $this->render('welcome');
        } else {
            $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}?page=home");
        }

    }

    public function createAccount()
    {

        $mapper = new UserMapper();

        if ($this->isPost()) {

            if ($mapper->checkIfEmailExists($_POST['email'])) {
                return $this->render('createAccount', ['message' => ['User with that email already exists']]);
            } else {
                $mapper->setUser($_POST['name'], $_POST['surname'], $_POST['nick'], $_POST['email'], md5($_POST['password']));

                $url = "http://$_SERVER[HTTP_HOST]/";
                header("Location: {$url}?page=welcome");
                exit();
            }
        }

        $this->render('createAccount');
    }

    public function login()
    {
        $mapper = new UserMapper();

        $user = null;

        if ($this->isPost()) {

            $user = $mapper->getUser($_POST['email']);

            if (!$user or $user->getPassword() !== md5($_POST['password'])) {
                return $this->render('login', ['message' => ['Incorrect data entered']]);
            } else {
                $_SESSION["id"] = $user->getId();
                $_SESSION["email"] = $user->getEmail();
                $_SESSION["role"] = $user->getRole();
                $url = "http://$_SERVER[HTTP_HOST]/";

                header("Location: {$url}?page=home");
                exit();
            }
        }

        $this->render('login');
    }

    public function logout()
    {
        session_unset();
        session_destroy();
        $url = "http://$_SERVER[HTTP_HOST]/";
        header("Location: {$url}?page=welcome");
    }

    public function goToMyAccountPanel()
    {
        $url = "http://$_SERVER[HTTP_HOST]/";

        if ($_SESSION["role"] == ROLE_ADMIN) {
            header("Location: {$url}?page=admin");
            exit();
        }
        if (isset($_SESSION["role"])) {
            header("Location: {$url}?page=myAccount");
            exit();
        }
    }
}