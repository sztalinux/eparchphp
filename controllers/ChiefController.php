<?php
require_once 'AppController.php';

require_once __DIR__.'/../model/User.php';
require_once __DIR__.'/../model/UserMapper.php';

class ChiefController extends AppController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if($this->roleValidation(ROLE_CHIEF)){
            exit();
        }
        $user = new UserMapper();
        $this->render('index', ['user' => $user->getUser($_SESSION['email'])]);
    }

    public function subordinates(): void
    {
        if($this->roleValidation(ROLE_CHIEF)){
            exit();
        }

        $user = new UserMapper();

        header('Content-type: application/json');
        http_response_code(200);

        echo $user->getSubordinates(ROLE_CHIEF) ? json_encode($user->getSubordinates(ROLE_CHIEF)) : '';
    }

    public function menu()
    {
        if($this->roleValidation(ROLE_CHIEF)){
            exit();
        }
        $this->render('menu');
    }


}