<?php
const SERVERNAME = 'localhost';
const USERNAME = 'root';
const PASSWORD = '';
const DB_NAME = 'eparch';

//role
const ROLE_ADMIN = 1;
const ROLE_CHIEF = 2;
const ROLE_ELDER = 3;
const ROLE_SCAB = 4;

const ORDER_GOOD = 1;
const ORDER_BAD = 2;

const AWARD_POTATO = 1;