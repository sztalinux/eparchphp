<?php

require_once 'controllers/DefaultController.php';
require_once 'controllers/AdminController.php';
require_once 'controllers/HomeController.php';
require_once 'controllers/MyAccountController.php';
require_once 'controllers/ChiefController.php';
require_once 'controllers/ElderController.php';
require_once 'controllers/ScabController.php';

class Routing
{
    public $routes = [];

    public function __construct()
    {
        $this->routes = [
            'home' => [
                'controller' => 'HomeController',
                'action' => 'index'
            ],
            'login' => [
                'controller' => 'DefaultController',
                'action' => 'login'
            ],
            'welcome' => [
                'controller' => 'DefaultController',
                'action' => 'welcome'
            ],
            'createAccount' => [
                'controller' => 'DefaultController',
                'action' => 'createAccount'
            ],
            'logout' => [
                'controller' => 'DefaultController',
                'action' => 'logout'
            ],
            'myAccount' => [
                'controller' => 'MyAccountController',
                'action' => 'index'
            ],
            'myAccount_menu' => [
                'controller' => 'MyAccountController',
                'action' => 'menu'
            ],
            'admin' => [
                'controller' => 'AdminController',
                'action' => 'index'
            ],
            'admin_users' => [
                'controller' => 'AdminController',
                'action' => 'users'
            ],
            'admin_delete_user' => [
                'controller' => 'AdminController',
                'action' => 'userDelete'
            ],
            'scab_orders' => [
                'controller' => 'ScabController',
                'action' => 'orders'
            ],
            'scab_orders_view' => [
                'controller' => 'ScabController',
                'action' => 'ordersListView'
            ],
            'scab_events' => [
                'controller' => 'ScabController',
                'action' => 'events'
            ],
            'scab_menu' => [
                'controller' => 'ScabController',
                'action' => 'menu'
            ],
            'elder_subordinates' => [
                'controller' => 'ElderController',
                'action' => 'subordinates'
            ],
            'elder_subordinates_view' => [
                'controller' => 'ElderController',
                'action' => 'subordinatesView'
            ],
            'elder_menu' => [
                'controller' => 'ElderController',
                'action' => 'menu'
            ],
            'chief_subordinates' => [
                'controller' => 'ChiefController',
                'action' => 'subordinates'
            ],
            'chief_menu' => [
                'controller' => 'ChiefController',
                'action' => 'menu'
            ],
            'goToMyAccountPanel' => [
                'controller' => 'DefaultController',
                'action' => 'goToMyAccountPanel'
            ]

        ];
    }

    public function run()
    {
        $page = isset($_GET['page'])
            && isset($this->routes[$_GET['page']]) ? $_GET['page'] : 'welcome';

        if ($this->routes[$page]) {
            $class = $this->routes[$page]['controller'];
            $action = $this->routes[$page]['action'];

            $object = new $class;
            $object->$action();
        }
    }

}