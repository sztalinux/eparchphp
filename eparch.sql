-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 21, 2019 at 07:56 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eparch`
--

-- --------------------------------------------------------

--
-- Table structure for table `achievements`
--

CREATE TABLE `achievements` (
  `id` int(255) NOT NULL,
  `usersID` int(255) NOT NULL,
  `ordersID` int(255) NOT NULL,
  `achieveDate` date NOT NULL,
  `description` text COLLATE utf8_polish_ci,
  `author` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Dumping data for table `achievements`
--

INSERT INTO `achievements` (`id`, `usersID`, `ordersID`, `achieveDate`, `description`, `author`) VALUES
(1, 1, 1, '2019-02-10', 'Za nic.', 4),
(2, 1, 2, '2019-02-10', 'Też za nic.', 3);

-- --------------------------------------------------------

--
-- Stand-in structure for view `achievementstodisplay`
-- (See below for the actual view)
--
CREATE TABLE `achievementstodisplay` (
`id` int(255)
,`usersID` int(255)
,`orderID` int(20)
,`name` varchar(255)
,`image` varchar(255)
,`achieveDate` date
,`description` text
,`author` int(255)
);

-- --------------------------------------------------------

--
-- Table structure for table `awards`
--

CREATE TABLE `awards` (
  `id` int(255) NOT NULL,
  `name` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(20) NOT NULL,
  `name` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `name`, `image`) VALUES
(1, 'Medal Dobrego Parcha', 'good.jpg'),
(2, 'Medal Zlego Parcha', 'bad.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(20) NOT NULL,
  `name` varchar(255) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `name`) VALUES
(1, 'admin'),
(2, 'wodz'),
(3, 'starszy'),
(4, 'parch');

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` int(255) NOT NULL,
  `name` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `description` text COLLATE utf8_polish_ci,
  `chief` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `name`, `description`, `chief`) VALUES
(1, 'Bractwo Lugiów', 'Zakopiańska drużyna wojów, Lugiowie, na czele z jaśnie panującym Brahanem.', 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(255) NOT NULL,
  `name` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `nick` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `role` int(20) NOT NULL,
  `team` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `surname`, `nick`, `email`, `password`, `role`, `team`) VALUES
(1, 'parch', 'parch', 'parch', 'parch@parch.com', '5e6f990e81ee67df3bd6466393c94caa', 4, NULL),
(2, 'Albert', 'Stanula', 'Bromir', 'albertstanula97@gmail.com', 'c6ec20b2664d53021b7310c80d6ae903', 1, NULL),
(3, 'starszy', 'starszy', 'starszy', 'starszy@starszy.com', '7b95e9e9c3f653a6f6f2acf2a4dee634', 3, NULL),
(4, 'wodz', 'wodz', 'wodz', 'wodz@wodz.com', 'ae67956d365edac39ba7d8af29abf1aa', 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `usersawards`
--

CREATE TABLE `usersawards` (
  `id` int(255) NOT NULL,
  `usersID` int(255) NOT NULL,
  `awardsID` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Structure for view `achievementstodisplay`
--
DROP TABLE IF EXISTS `achievementstodisplay`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `achievementstodisplay`  AS  select `a`.`id` AS `id`,`a`.`usersID` AS `usersID`,`o`.`id` AS `orderID`,`o`.`name` AS `name`,`o`.`image` AS `image`,`a`.`achieveDate` AS `achieveDate`,`a`.`description` AS `description`,`a`.`author` AS `author` from (`achievements` `a` join `orders` `o` on((`a`.`ordersID` = `o`.`id`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `achievements`
--
ALTER TABLE `achievements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `usersID` (`usersID`),
  ADD KEY `ordersID` (`ordersID`),
  ADD KEY `author` (`author`);

--
-- Indexes for table `awards`
--
ALTER TABLE `awards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`),
  ADD KEY `chief` (`chief`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role` (`role`),
  ADD KEY `team` (`team`);

--
-- Indexes for table `usersawards`
--
ALTER TABLE `usersawards`
  ADD PRIMARY KEY (`id`),
  ADD KEY `usersID` (`usersID`),
  ADD KEY `awardsID` (`awardsID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `achievements`
--
ALTER TABLE `achievements`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `awards`
--
ALTER TABLE `awards`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `usersawards`
--
ALTER TABLE `usersawards`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `achievements`
--
ALTER TABLE `achievements`
  ADD CONSTRAINT `achievements_ibfk_1` FOREIGN KEY (`usersID`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `achievements_ibfk_2` FOREIGN KEY (`ordersID`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `achievements_ibfk_3` FOREIGN KEY (`author`) REFERENCES `users` (`id`);

--
-- Constraints for table `teams`
--
ALTER TABLE `teams`
  ADD CONSTRAINT `teams_ibfk_1` FOREIGN KEY (`chief`) REFERENCES `users` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role`) REFERENCES `role` (`id`),
  ADD CONSTRAINT `users_ibfk_2` FOREIGN KEY (`team`) REFERENCES `teams` (`id`);

--
-- Constraints for table `usersawards`
--
ALTER TABLE `usersawards`
  ADD CONSTRAINT `usersawards_ibfk_1` FOREIGN KEY (`usersID`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `usersawards_ibfk_2` FOREIGN KEY (`awardsID`) REFERENCES `awards` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
