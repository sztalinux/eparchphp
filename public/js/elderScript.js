$(document).ready(function () {
    console.log( "ready!" );
});

function getElderSubordinates() {

    getElderSubordinatesView();
    var lp = 1;
    $.ajax({
        url: '/?page=elder_subordinates',
        dataType: 'json',
        success: function (result) {
            console.log(result);
        },
        error: function () {
            console.log("error");
        }
    })
        .done((res) => {
            res.forEach(el => {
                $("tbody#elderSubordinates").append(`<tr>
                    <td>${lp}</td>
                    <td>${el.userName}</td>
                    <td>${el.userNick}</td>
                    <td>${el.userSurname}</td>
                    <td>${el.userEmail}</td>
                    <td>${el.roleName}</td>
                    <td>
                    <button class="btn giveOrder" type="button" onclick="giveOrderPopup(${el})">
                        <p> PRZYZNAJ ORDER </p>
                    </button>
                    </td>
                    </tr>`);
                lp++;
                lala();
            })
        });
}

function getElderSubordinatesView(){
    $.ajax({
        url: '/?page=elder_subordinates_view',
        dataType: 'html'
    })
        .done(function (res) {
            $("div#userField").html(res);
        })
        .fail(function () {
            console.log("error");
        });
}
function lala($el){
    return $el;
}

function giveOrderPopup($el) {

    $('.orderPopup').show();

    $('.orderPopup').click(function(){
        $('.orderPopup').hide();
    });
    $('.popupCloseButton').click(function(){
        $('.orderPopup').hide();
    });


}