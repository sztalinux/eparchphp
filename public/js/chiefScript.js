$(document).ready(function () {
    $('.panel-header').html('SIGN IN');
});

function getChiefSubordinates() {
    const $list = $('.users-list');

    $.ajax({
        url: '/?page=chief_subordinates',
        dataType: 'json',
        success: function (result) {
            console.log(result);
        },
        error: function () {
            console.log("error");
        }
    })
        .done((res) => {

            $list.empty();
            //robimy pętlę po zwróconej kolekcji
            //dołączając do tabeli kolejne wiersze
            res.forEach(el => {
                $list.append(`<tr>
                    <td>${el.name}</td>
                    <td>${el.surname}</td>
                    <td>${el.email}</td>
                    <td>${el.role}</td>
                    <td>
                    <button class="btn btn-danger" type="button" onclick="deleteUser(${el.id})">
                        <i class="material-icons">delete_forever</i>
                    </button>
                    </td>
                    </tr>`);
            })
        });
}
