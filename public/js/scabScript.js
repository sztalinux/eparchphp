$(document).ready(function () {
    $('.panel-header').html('SIGN IN');
});

function getOrdersList() {
    const imgSrc = "../../images/";

    getOrdersListView();
    var lp = 1;
    $.ajax({
        url: '/?page=scab_orders',
        dataType: 'json'
    })
        .done(function (res) {
            res.forEach(el => {
                // document.getElementById("orderImg").src=imgSrc+el.image;
                $("tbody#ordersList").append(`<tr onclick="display()">
                    <td>${lp}</td>
                    <td><img id="orderImg" class="img-fluid" src=${imgSrc}${el.orderImage}></td>
                    <td>${el.orderName}</td>
                    <td>${el.achieveDate}</td>
                    <td>${el.authorNick}</td>
                    <td>${el.description}</td>
                    
                    </tr>`);
                lp++;
            })
        })
        .fail(function () {
            console.log("error");
        });

}

function getOrdersListView(){
    $.ajax({
        url: '/?page=scab_orders_view',
        dataType: 'html'
    })
        .done(function (res) {
            $("div#userField").html(res);
        })
        .fail(function () {
            console.log("error");
        });
}
