<!DOCTYPE html>
<html lang="en">


<div id="userData">
    <table class="table table-hover table-responsive-sm">
        <thead>
        <tr>
            <th>Name</th>
            <th>Surname</th>
            <th>Nick</th>
            <th>Role</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><?= $user->getName(); ?></td>
            <td><?= $user->getSurname(); ?></td>
            <td><?= $user->getNick(); ?></td>
            <td><?= $user->getRoleName(); ?></td>
        </tr>
        </tbody>
    </table>
</div>


</html>