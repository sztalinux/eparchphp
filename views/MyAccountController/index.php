<!DOCTYPE html>
<html>

<?php include(dirname(__DIR__) . '/head.html'); ?>

<body onload="loadUserMenu(<?php $_SESSION['email'] ?>)">
<?php
include(dirname(__DIR__) . '/navbars/myAccountNavbar.html');
?>
<div class="wrapper">
    <div class="row"><!--dodac w style tło do tego rowa-->
        <div class="col-sm-2 padding">
            <?php
            require_once('userImage.php');
            //        require_once('../model/UserMapper.php');
            ?>
        </div>
        <div class="col-sm-10">
            <?php
            require_once('userData.php');
            ?>
        </div>
    </div>
    <div class="row">
        <div id="userMenu" class="col-sm-2 padding">
        </div>

        <div id="userField" class="col-sm-10 padding">
        </div>
    </div>

    <?php
    require_once('userPopup.php');
    ?>




</div>

</body>
</html>