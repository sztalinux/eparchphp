<!DOCTYPE html>
<html>

<?php include(dirname(__DIR__).'/head.html'); ?>

<body>
<?php
include(dirname(__DIR__) . '/navbars/myAccountNavbar.html');
?>
<div class="container">
    <div class="row">
        <div class="row col-sm-10 offset-1">
            <?php
            require_once(dirname(__DIR__) . '/userData.php');
            ?>
        </div>
        <div>
            <h4 class="mt-4">Twoi ludzie:</h4>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Surname</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody class="users-list">
                </tbody>
            </table>

            <button class="btn btn-dark btn-lg" type="button" onclick="getChiefSubordinates()">Get all users</button>
        </div>

    </div>
</div>

</body>
</html>