<div id="orders" class="container">
    <div class="row">
        <div class="col-sm-12" style="padding-top: 5px">
            <table id="ordersTable" class="table table-hover table-responsive-sm">
                <thead>
                <tr>
                    <th>LP</th>
                    <th>IMG</th>
                    <th>NAZWA ORDERU</th>
                    <th>DATA UZYSKANIA</th>
                    <th>HONORUJĄCY</th>
                    <th>OPIS</th>
                </tr>
                </thead>
                <tbody id="ordersList">
                </tbody>
            </table>
        </div>
    </div>
</div>

