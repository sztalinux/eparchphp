<!DOCTYPE html>
<html>

<?php include(dirname(__DIR__).'/head.html'); ?>

<body>
<?php include(dirname(__DIR__) . '/navbars/homeNavbar.html'); ?>

<div class="container-fluid">
    <div class="row">
        <h1 class="col-12">HOMEPAGE</h1>
        <p>
            <?php if(isset($text)): ?>
            <?php foreach($text as $item): ?>
        <div><?= $item ?></div>
        <?php endforeach; ?>
        <?php endif; ?>
        </p>
        <?php
        if(isset($_SESSION) && !empty($_SESSION)) {
            print_r($_SESSION);
        }
        ?>
    </div>
</div>

</body>
</html>