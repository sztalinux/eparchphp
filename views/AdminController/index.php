<!DOCTYPE html>
<html>

<?php include(dirname(__DIR__).'/head.html');
//header('Access-Control-Allow-Origin: *');?>

<body>
<div class="container">
    <div class="row">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Name</th>
                <th>Surname</th>
                <th>Email</th>
                <th>Role</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><?= $user->getName(); ?></td>
                <td><?= $user->getSurname(); ?></td>
                <td><?= $user->getEmail(); ?></td>
                <td><?= $user->getRole(); ?></td>
                <td>-</td>
            </tr>
            </tbody>
            <tbody class="users-list">
            </tbody>
        </table>

        <button class="btn btn-dark btn-lg" type="button" onclick="getAllUsers()">Get all users</button>
    </div>
</div>

</body>
</html>