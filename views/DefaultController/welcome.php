<!DOCTYPE html>
<html>

<?php include(dirname(__DIR__).'/head.html'); ?>

<body>
<?php include(dirname(__DIR__) . '/navbars/welcomeNavbar.html'); ?>

<div class="container">
    <div class="row">
        <div class="col-sm-6 offset-sm-3">
            <h1>WELCOME</h1>
            <hr>
            <?php if(isset($message)): ?>
                <?php foreach($message as $item): ?>
                    <div><?= $item ?></div>
                <?php endforeach; ?>
            <?php endif; ?>
    </div>
</div>

</body>
</html>