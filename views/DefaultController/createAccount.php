<!DOCTYPE html>
<html>

<?php
include(dirname(__DIR__).'/head.html');
?>

<body>
<?php
include(dirname(__DIR__) . '/navbars/createAccountNavbar.html');
?>
<div class="container">
    <div class="row">
        <div class="col-sm-6 offset-sm-3">
            <h1>Create Account</h1>
            <hr>
            <?php if(isset($message)): ?>
                <?php foreach($message as $item): ?>
                    <div><?= $item ?></div>
                <?php endforeach; ?>
            <?php endif; ?>

            <form action="?page=createAccount" method="POST">

                <div class="form-group row">
                    <label for="inputName" class="col-sm-1 col-form-label">
                        <i class="material-icons md-48">person_outline</i>
                    </label>
                    <div class="col-sm-11">
                        <input type="text" class="form-control" id="inputName" name="name" placeholder="Name" required/>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputSurname" class="col-sm-1 col-form-label">
                        <i class="material-icons md-48">person</i>
                    </label>
                    <div class="col-sm-11">
                        <input type="text" class="form-control" id="inputSurname" name="surname" placeholder="Surname" required/>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputNick" class="col-sm-1 col-form-label">
                        <i class="material-icons md-48">whatshot</i>
                    </label>
                    <div class="col-sm-11">
                        <input type="text" class="form-control" id="inputNick" name="nick" placeholder="Nick" required/>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputEmail" class="col-sm-1 col-form-label">
                        <i class="material-icons md-48">email</i>
                    </label>
                    <div class="col-sm-11">
                        <input type="email" class="form-control" id="inputEmail" name="email" placeholder="Email" required/>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-1 col-form-label">
                        <i class="material-icons md-48">lock</i>
                    </label>
                    <div class="col-sm-11">
                        <input type="password" name="password" class="form-control" id="inputPassword" placeholder="Password"required/>
                    </div>
                </div>

                <input type="submit" value="Create" class="btn btn-primary btn-lg float-right" />
            </form>
        </div>
    </div>
</div>

</body>
</html>
